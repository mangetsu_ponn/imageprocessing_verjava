import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImageProcessing extends Application {
	StackPane sp_imageSpace;
	int imageWidth;
	int imageHeight;
	WritableImage wImg;
	PixelWriter writer;
	WritablePixelFormat<IntBuffer> format;
	int[] beforeProcessingPixels;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox root = new VBox();
		sp_imageSpace = new StackPane();
		sp_imageSpace.setAlignment(Pos.TOP_LEFT);

		MenuBar menuBar = new MenuBar();

		Menu menu_file = new Menu("ファイル");
		menuBar.getMenus().add(menu_file);
		Menu menu_run = new Menu("実行");
		menuBar.getMenus().add(menu_run);

		MenuItem mi_open = new MenuItem("開く");
		menu_file.getItems().add(mi_open);
		MenuItem mi_save = new MenuItem("保存");
		menu_file.getItems().add(mi_save);
		MenuItem mi_sharpening = new MenuItem("鮮鋭化処理");
		menu_run.getItems().add(mi_sharpening);
		MenuItem mi_smoothing = new MenuItem("平滑化処理");
		menu_run.getItems().add(mi_smoothing);
		MenuItem mi_inversion = new MenuItem("ネガ･ポジ反転");
		menu_run.getItems().add(mi_inversion);
		MenuItem mi_grayscale = new MenuItem("グレースケール変換");
		menu_run.getItems().add(mi_grayscale);
		MenuItem mi_binary = new MenuItem("2値化処理");
		menu_run.getItems().add(mi_binary);

		mi_open.setOnAction(new FileOpeningEventHandler());
		mi_save.setOnAction(new FileSavingEventHandler());
		mi_sharpening.setOnAction(new SharpeningImageCreationEventHandler());
		mi_smoothing.setOnAction(new SmoothingImageCreationEventHandler());
		mi_inversion.setOnAction(new InversionImageCreationEventHandler());
		mi_grayscale.setOnAction(new GrayscaleImageCreationEventHandler());
		mi_binary.setOnAction(new BinaryImageCreationEventHandler());

		root.getChildren().addAll(menuBar, sp_imageSpace);

		Scene scene = new Scene(root, 800, 500);

		primaryStage.setScene(scene);
		primaryStage.setTitle("画像処理");
		primaryStage.show();
	}

	// ファイル(画像)を開く処理
	class FileOpeningEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("イメージファイル", "*.png; *.jpg; *.gif"));
			fc.setTitle("ファイルを開く");

			File file = fc.showOpenDialog(new Stage());

			if(file != null && file.isFile()) {
				String path = file.getAbsolutePath();
				Image image = new Image(Paths.get(path).toUri().toString());
				PixelReader reader = image.getPixelReader();
				imageWidth = (int)image.getWidth();
				imageHeight = (int)image.getHeight();

				wImg = new WritableImage(imageWidth, imageHeight);
				writer = wImg.getPixelWriter();
				format = WritablePixelFormat.getIntArgbInstance();
				beforeProcessingPixels = new int[imageWidth * imageHeight];
				reader.getPixels(0, 0, imageWidth, imageHeight, format, beforeProcessingPixels, 0, imageWidth);
				writer.setPixels(0, 0, imageWidth, imageHeight, format, beforeProcessingPixels, 0, imageWidth);

				ImageView imageView = new ImageView(wImg);

				if(sp_imageSpace.getChildren().size() == 1) {
					sp_imageSpace.getChildren().remove(0, 1);
				}

				sp_imageSpace.getChildren().add(imageView);
			}
		}
	}

	// ファイル(画像)を保存する処理
	class FileSavingEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("PNGファイル", "*.png"));
			fc.setTitle("ファイルを保存");

			File file = fc.showSaveDialog(new Stage());

			if(file != null) {
				WritableImage wImg = new WritableImage(imageWidth, imageHeight);
				SnapshotParameters sp = new SnapshotParameters();
				sp.setFill(Color.TRANSPARENT);

				try {
					ImageIO.write(SwingFXUtils.fromFXImage(
						sp_imageSpace.snapshot(sp, wImg), null), "png", file);
				} catch(IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	// 24近傍鮮鋭化フィルタ(5画素×5画素)による鮮鋭化処理(アンシャープマスキング処理)
	class SharpeningImageCreationEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			// 鮮鋭化処理後のピクセルを設定する配列を用意する
			int[] afterProcessingPixels = new int[imageWidth * imageHeight];

			// ピクセル操作(鮮鋭化処理)
			for(int y = 0; y < imageHeight; y++) {
				for(int x = 0; x < imageWidth; x++) {
					int index = (imageWidth * y) + x;
					int alpha = 0, red = 0, green = 0, blue = 0;

					// (x, y)が画像の外枠以外の場合に鮮鋭化処理をする
					if((x == 0) || (x == 1) || (x == (imageWidth - 1)) || (x == (imageWidth - 2))
							|| (y == 0) || (y == 1) || (y == (imageHeight - 1)) || (y == (imageHeight - 2))) {
						afterProcessingPixels[index] = beforeProcessingPixels[index];
					} else {
						int pixel;

						for(int n = -2; n < 3; n++) {
							for(int m = -2; m < 3; m++) {
								int pointer = index + (imageWidth * n) + m;

								// ピクセルを取得する
								pixel = beforeProcessingPixels[pointer];

								// アルファ成分、赤成分、緑成分、青成分の各成分を取得、作成
								if(m == 0 && n == 0) {
									alpha = ((pixel >> 24) & 0xFF);
									red += ((pixel >> 16) & 0xFF) * 49 / 25;
									green += ((pixel >> 8) & 0xFF) * 49 / 25;
									blue += (pixel & 0xFF) * 49 / 25;
								} else {
									red -= ((pixel >> 16) & 0xFF) / 25;
									green -= ((pixel >> 8) & 0xFF) / 25;
									blue -= (pixel & 0xFF) / 25;
								}
							}
						}

						if(red < 0) { red = 0; }
						if(red > 255) { red = 255; }
						if(green < 0) { green = 0; }
						if(green > 255) { green = 255; }
						if(blue < 0) { blue = 0; }
						if(blue > 255) { blue = 255; }

						// 鮮鋭化処理をしたピクセルを設定する
						pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
						afterProcessingPixels[index] = pixel;
					}
				}
			}

			writer.setPixels(0, 0, imageWidth, imageHeight, format, afterProcessingPixels, 0, imageWidth);

			ImageView imageView = new ImageView(wImg);

			if(sp_imageSpace.getChildren().size() == 1) {
				sp_imageSpace.getChildren().remove(0, 1);
			}

			sp_imageSpace.getChildren().add(imageView);
		}
	}

	// 5画素×5画素の平均化フィルタによる平滑化処理(ぼかし処理)
	class SmoothingImageCreationEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			// 平滑化処理後のピクセルを設定する配列を用意する
			int[] afterProcessingPixels = new int[imageWidth * imageHeight];

			// ピクセル操作(平滑化処理)
			for(int y = 0; y < imageHeight; y++) {
				for(int x = 0; x < imageWidth; x++) {
					int index = (imageWidth * y) + x;
					int alpha = 0, red = 0, green = 0, blue = 0;

					// (x, y)が画像の外枠以外の場合に平滑化処理をする
					if((x == 0) || (x == 1) || (x == (imageWidth - 1)) || (x == (imageWidth - 2))
							|| (y == 0) || (y == 1) || (y == (imageHeight - 1)) || (y == (imageHeight - 2))) {
						afterProcessingPixels[index] = beforeProcessingPixels[index];
					} else {
						int pixel;

						for(int n = -2; n < 3; n++) {
							for(int m = -2; m < 3; m++) {
								int pointer = index + (imageWidth * n) + m;

								// ピクセルを取得する
								pixel = beforeProcessingPixels[pointer];

								// アルファ成分、赤成分、緑成分、青成分の各成分を取得、作成
								if(m == 0 && n == 0) {
									alpha = (pixel >> 24) & 0xFF;
								}

								red += ((pixel >> 16) & 0xFF) / 25;
								green += ((pixel >> 8) & 0xFF) / 25;
								blue += (pixel & 0xFF) / 25;
							}
						}

						// 平滑化処理をしたピクセルを設定する
						pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
						afterProcessingPixels[index] = pixel;
					}
				}
			}

			writer.setPixels(0, 0, imageWidth, imageHeight, format, afterProcessingPixels, 0, imageWidth);

			ImageView imageView = new ImageView(wImg);

			if(sp_imageSpace.getChildren().size() == 1) {
				sp_imageSpace.getChildren().remove(0, 1);
			}

			sp_imageSpace.getChildren().add(imageView);
		}
	}

	// ネガ・ポジ反転
	class InversionImageCreationEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			// ネガ・ポジ反転の処理後のピクセルを設定する配列を用意する
			int[] afterProcessingPixels = new int[imageWidth * imageHeight];

			// ピクセル操作(ネガ・ポジ反転の処理)
			for(int y = 0; y < imageHeight; y++) {
				for(int x = 0; x < imageWidth; x++) {
					int index = (y * imageWidth) + x;

					// ピクセルを取得する
					int pixel = beforeProcessingPixels[index];

					// アルファ成分、赤成分、緑成分、青成分の各成分を取得、作成
					int alpha = (pixel >> 24) & 0xFF;
					int red = 255 - ((pixel >> 16) & 0xFF);
					int green = 255 - ((pixel >> 8) & 0xFF);
					int blue = 255 - (pixel & 0xFF);

					// ネガ・ポジ反転の処理をしたピクセルを設定する
					pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
					afterProcessingPixels[index] = pixel;
				}
			}

			writer.setPixels(0, 0, imageWidth, imageHeight, format, afterProcessingPixels, 0, imageWidth);

			ImageView imageView = new ImageView(wImg);

			if(sp_imageSpace.getChildren().size() == 1) {
				sp_imageSpace.getChildren().remove(0, 1);
			}

			sp_imageSpace.getChildren().add(imageView);
		}
	}

	// NTSC系加重平均法によるグレースケール変換(モノクロ変換)
	class GrayscaleImageCreationEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			// グレースケール変換の処理後のピクセルを設定する配列を用意する
			int[] afterProcessingPixels = new int[imageWidth * imageHeight];

			// ピクセル操作(グレースケール変換の処理)
			for(int y = 0; y < imageHeight; y++) {
				for(int x = 0; x < imageWidth; x++) {
					int index = (y * imageWidth) + x;

					// ピクセルを取得する
					int pixel = beforeProcessingPixels[index];

					// アルファ成分、赤成分、緑成分、青成分の各成分を取得、作成
					int alpha = (pixel >> 24) & 0xFF;
					int red = (pixel >> 16) & 0xFF;
					int green = (pixel >> 8) & 0xFF;
					int blue = pixel & 0xFF;

					int luma = (int)(0.298912 * (double)red + 0.586611 * (double)green + 0.114478 * (double)blue);

					red = luma;
					green = luma;
					blue = luma;

					// グレースケール変換の処理をしたピクセルを設定する
					pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
					afterProcessingPixels[index] = pixel;
				}
			}

			writer.setPixels(0, 0, imageWidth, imageHeight, format, afterProcessingPixels, 0, imageWidth);

			ImageView imageView = new ImageView(wImg);

			if(sp_imageSpace.getChildren().size() == 1) {
				sp_imageSpace.getChildren().remove(0, 1);
			}

			sp_imageSpace.getChildren().add(imageView);
		}
	}

	// 閾値が127の場合の2値化処理
	class BinaryImageCreationEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			// 2値化処理後のピクセルを設定する配列を用意する
			int[] afterProcessingPixels = new int[imageWidth * imageHeight];

			// ピクセル操作(2値化処理)
			for(int y = 0; y < imageHeight; y++) {
				for(int x = 0; x < imageWidth; x++) {
					int index = (y * imageWidth) + x;

					// ピクセルを取得する
					int pixel = beforeProcessingPixels[index];

					// アルファ成分、赤成分、緑成分、青成分の各成分を取得、作成
					int alpha = (pixel >> 24) & 0xFF;
					int red = (pixel >> 16) & 0xFF;
					int green = (pixel >> 8) & 0xFF;
					int blue = pixel & 0xFF;

					int luma = (int)(0.298912 * (double)red + 0.586611 * (double)green + 0.114478 * (double)blue);

					if(luma <= 127) {
						red = 0;
						green = 0;
						blue = 0;
					} else {
						red = 255;
						green = 255;
						blue = 255;
					}

					// 2値化処理をしたピクセルを設定する
					pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
					afterProcessingPixels[index] = pixel;
				}
			}

			writer.setPixels(0, 0, imageWidth, imageHeight, format, afterProcessingPixels, 0, imageWidth);

			ImageView imageView = new ImageView(wImg);

			if(sp_imageSpace.getChildren().size() == 1) {
				sp_imageSpace.getChildren().remove(0, 1);
			}

			sp_imageSpace.getChildren().add(imageView);
		}
	}
}
